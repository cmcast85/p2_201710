package test.estructuras;

import junit.framework.TestCase;
import model.data_structures.LinearProbingHashST;
import model.data_structures.MaxPQ;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateHash;
import model.data_structures.Stack;
import model.logic.GeneradorDatos;
import model.vo.VOPrueba;

public class Test_estructuras extends TestCase {

	private SeparateHash<Integer, String> sh ;
	
	private LinearProbingHashST<Integer, String> lh;
	
	private Queue<String> queue;
	
	private Stack<String> stack;
	
	private RedBlackBST<Integer, String> rb;
	
	private MaxPQ<VOPrueba> pruebas;
	
	private GeneradorDatos gen;
	
	
	
	protected void setUp() throws Exception {
		sh = new SeparateHash<Integer,String >();
		lh = new LinearProbingHashST<Integer, String>();
		
		stack = new Stack<String>();
		queue = new Queue<String>();
		
		rb = new RedBlackBST<>();
		
		pruebas = new MaxPQ<VOPrueba>(100);
		
		gen = new GeneradorDatos();
		super.setUp();
	}

	public void testMAxPQ()
	{
		
		
		int [] anos = new int[100];
		anos = gen.generarAno(100);
		
		String[] nombres = new String[100];
		nombres = gen.generarCadena(100);
		
		
		for(int i = 0;i<100;i++ )
		{
		VOPrueba prueba = new VOPrueba();
		prueba.setAno(anos[i]);
		prueba.setNombre(nombres[i]);
		
		pruebas.insert(prueba);
		}
		Maximos();
	}
	
	private void Maximos(){
		VOPrueba[] temp = new VOPrueba[100];
		int i = 0;
		while(!pruebas.isEmpty()){
			temp[i] = pruebas.delMax();
			if(0<i && i<99){
				assertTrue(temp[i-1].compareTo(temp[i])  >0);
			}
			i++;
		}
		
	}
	
	
	public void testSPush(){
		try {
			setUp();
			stack.push("Test1");
			assertEquals("Test1", stack.peek());
			stack.push("Test2");
			assertEquals("Test2",stack.peek());
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testQPut(){
		try {
			setUp();
			queue.put("Test1");
			assertEquals("Test1", queue.get(0));
			queue.put("Test2");
			assertEquals("Test2",queue.get(1));
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testQPull(){
		try {
			setUp();
			queue.put("Test1");
			queue.put("Test2");
			queue.put("Test3");
			
			queue.pull();
			assertEquals("Test2",queue.get(0));
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testSPop(){
		try {
			setUp();
			stack.push("Test1");
			stack.push("Test2");
			stack.push("Test3");
			
			stack.pop();
			assertTrue(stack.size() ==2);
			assertEquals("Test2", stack.get(0));
			} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	
	public void testSHPut(){
		try {
			setUp();
			sh.put(1, "Prueba");
			sh.put(2, "p2");
			sh.put(3, "p3");
			
			assertTrue(sh.get(1).equals("Prueba"));
			assertTrue(sh.get(2).equals("p2"));
			assertTrue(sh.get(3).equals("p3"));
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
		
		
		
	}
	
	public void testLHPut(){
		try {
			setUp();
			lh.put(1, "aj1");
			lh.put(2, "aj2");
			lh.put(3, "aj3");
			lh.put(4, "aj4");
			
			assertTrue(lh.get(1).equals("aj1"));
			assertTrue(lh.get(2).equals("aj2"));
			assertTrue(lh.get(3).equals("aj3"));
			assertTrue(lh.get(4).equals("aj4"));
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testSHDelete(){
		try {
			setUp();
			sh.put(1, "Prueba");
			sh.put(2, "p2");
			sh.put(3, "p3");
			
			sh.delete(2);
			
			assertTrue(sh.get(2)==null);
			assertTrue(sh.get(1).equals("Prueba"));
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testLHDelete(){
		try {
			setUp();
			lh.put(1, "aj1");
			lh.put(2, "aj2");
			lh.put(3, "aj3");
			lh.put(4, "aj4");
			
			lh.delete(2);
			
			assertTrue(lh.get(2)==null);
			assertTrue(lh.get(4).equals("aj4"));
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testSHSize(){
		try {
			setUp();
			sh.put(1, "Prueba");
			sh.put(2, "p2");
			sh.put(3, "p3");
			
			assertTrue(sh.size()==3);
			
			sh.delete(1);
			
			assertTrue(sh.size()==2);
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testLHSize(){
		try {
			setUp();
			lh.put(1, "aj1");
			lh.put(2, "aj2");
			lh.put(3, "aj3");
			lh.put(4, "aj4");
			
			assertTrue(lh.size()==4);
			
			lh.delete(1);
			
			assertTrue(lh.size()==3);
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testSHContains(){
		try {
			setUp();
			sh.put(1, "Prueba");
			sh.put(2, "p2");
			sh.put(3, "p3");
			
			Iterable<Integer> st = sh.keys();
			int j = 1;
			for(Integer i : st){
				assertTrue(i==j);
				j++;
			}
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
		
	}
	
	public void testLHContains(){

		try {
			setUp();
			lh.put(1, "aj1");
			lh.put(2, "aj2");
			lh.put(3, "aj3");
			lh.put(4, "aj4");
			
			Iterable<Integer> st = lh.keys();
			int j = 1;
			for(Integer i : st){
				assertTrue(i==j);
				j++;
			}
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testBST(){
		try {
			setUp();
			rb.put(1, "Test1");
			rb.put(7, "Test7");
			rb.put(8, "Test8");
			rb.put(3, "Test3");
			rb.put(10, "Test10");
			rb.put(4, "Test4");
			rb.put(9, "Test9");
			rb.put(5, "Test5");
			rb.put(2, "Test2");
			rb.put(6, "Test6");
			assertTrue(rb.check());
			
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	public void testBSTMax(){
		try {
			setUp();
			rb.put(1, "Test1");
			rb.put(7, "Test7");
			rb.put(8, "Test8");
			rb.put(3, "Test3");
			rb.put(10, "Test10");
			rb.put(4, "Test4");
			rb.put(9, "Test9");
			rb.put(5, "Test5");
			rb.put(2, "Test2");
			rb.put(6, "Test6");
			
			rb.deleteMax();
			
			assertTrue(rb.max() ==9);
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	public void testBSTMin(){
		try {
			setUp();
			rb.put(1, "Test1");
			rb.put(7, "Test7");
			rb.put(8, "Test8");
			rb.put(3, "Test3");
			rb.put(10, "Test10");
			rb.put(4, "Test4");
			rb.put(9, "Test9");
			rb.put(5, "Test5");
			rb.put(2, "Test2");
			rb.put(6, "Test6");
			
			rb.deleteMin();
			
			assertTrue(rb.min() ==2);
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	public void testBSTCeiling(){
		try {
			setUp();
			rb.put(1, "Test1");
			rb.put(7, "Test7");
			rb.put(8, "Test8");
			rb.put(3, "Test3");
			rb.put(10, "Test10");
			rb.put(4, "Test4");
			rb.put(9, "Test9");
			rb.put(5, "Test5");
			rb.put(2, "Test2");
			rb.put(6, "Test6");
			
			
			
			assertTrue(rb.ceiling(0) ==1);
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	public void testBSTFloor(){
		try {
			setUp();
			rb.put(1, "Test1");
			rb.put(7, "Test7");
			rb.put(8, "Test8");
			rb.put(3, "Test3");
			rb.put(10, "Test10");
			rb.put(4, "Test4");
			rb.put(9, "Test9");
			rb.put(5, "Test5");
			rb.put(2, "Test2");
			rb.put(6, "Test6");
			
			
			
			assertTrue(rb.floor(12) ==10);
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	public void testBSTaaj(){
		try {
			setUp();
			rb.put(1, "Test1");
			rb.put(7, "Test7");
			rb.put(8, "Test8");
			rb.put(3, "Test3");
			rb.put(10, "Test10");
			rb.put(4, "Test4");
			rb.put(9, "Test9");
			rb.put(5, "Test5");
			rb.put(2, "Test2");
			rb.put(6, "Test6");
			assertTrue(rb.height()<=5);
			
		} catch (Exception e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
}
