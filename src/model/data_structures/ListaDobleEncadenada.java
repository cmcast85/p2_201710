package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private Nodo<T> cabeza;
	private Nodo<T> posicionActual;
	private int tamano;
	
	public ListaDobleEncadenada (T primerElemento){
		cabeza = new Nodo<T>(primerElemento);
		posicionActual = cabeza;
		tamano = 1;
	}
	
	public ListaDobleEncadenada (){
		cabeza = null;
		posicionActual = null;
		tamano = 0;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Nodo<T> actual = null;
			@Override
			public boolean hasNext() {
				if (actual == null || actual.darSiguiente() != null) return true;
				else return false;
			}

			@Override
			public T next() {
				if (actual == null) actual = cabeza;
				else actual = actual.darSiguiente();
				return actual.darObjeto();
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(cabeza==null){
			cabeza = new Nodo<T>(elem);
			posicionActual = cabeza;
		}
		else {
			Nodo<T> actual = cabeza;
			while (actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			Nodo<T> siguiente = new Nodo<T>(elem, actual);
			actual.cambiarSiguiente(siguiente);
		}
		tamano++;
	}
	
	public void agregarElementoFinalNorepetido(T elem) {
		if(cabeza==null){
			cabeza = new Nodo<T>(elem);
			posicionActual = cabeza;
			tamano++;
		}
		else {
			Nodo<T> actual = cabeza;
			boolean repetido = false;
			while (actual.darSiguiente() != null && !repetido) {
				if(actual.darObjeto().equals(elem)){
					repetido = true;
				}
				actual = actual.darSiguiente();
			}
			if(!repetido){
				Nodo<T> siguiente = new Nodo<T>(elem);
				actual.cambiarSiguiente(siguiente);
				tamano++;
			}
		}
	}

	@Override
	public T darElemento(int pos) {
		Nodo<T> actual = cabeza;
		for (int i = 0; i < pos-1; i++) {
			if(actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			else return null;
		}
		return actual.darObjeto();
	}


	@Override
	public int darNumeroElementos() {
		return tamano;
	}

	public T darElementoPosicionActual() {
		return posicionActual.darObjeto();
	}

	public void restablecerActual() {
		posicionActual = cabeza;
	}

	public boolean avanzarSiguientePosicion() {
		if (posicionActual.darSiguiente() != null) {
			posicionActual = posicionActual.darSiguiente();
			return true;
		}
		else return false;
	}

	public boolean retrocederPosicionAnterior() {
		if (posicionActual.darAnterior() != null) {
			posicionActual = posicionActual.darAnterior();
			return true;
		}
		else return false;
	}
	
	public boolean isEmpty()
	{
		return tamano ==0;
	}

	public Nodo<T> darNodo(int pos) {
		Nodo<T> actual = cabeza;
		for (int i = 0; i < pos-1; i++) {
			if(actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			else return null;
		}
		return actual;
	}

	public boolean eliminarElemento(int pos) {
		Nodo<T> elem = darNodo(pos);
		boolean logro = false;
		if (tamano > 1) {
			if(elem.darAnterior() == null) {
				elem.darSiguiente().cambiarAnterior(null);
				cabeza = elem.darSiguiente();
				elem.cambiarSiguiente(null);
				logro = true;
			} else if (elem.darSiguiente() == null) {
				elem.darAnterior().cambiarSiguiente(null);
				elem.cambiarAnterior(null);
				logro = true;
			} else if (elem.darSiguiente() != null && elem.darAnterior() != null) {
				elem.darAnterior().cambiarSiguiente(elem.darSiguiente());
				elem.darSiguiente().cambiarAnterior(elem.darAnterior());
				elem.cambiarAnterior(null);
				elem.cambiarSiguiente(null);
				logro = true;
			}
		}
		else {
			cabeza = null;
			logro = true;
		}
		if (logro) tamano--;
		return logro;
	}

	@Override
	public boolean contains(T gen) {
		Nodo<T> actual = cabeza;
		
		while (actual.darSiguiente() != null) {
			if(actual.darObjeto().equals(gen)){
				return true;
			}
			actual = actual.darSiguiente();
		}
		return false;
	}

	
}
