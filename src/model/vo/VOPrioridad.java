package model.vo;

public class VOPrioridad implements Comparable<VOPrioridad> {
	
	private VOPelicula pelicula;
	private double prioridad;
	
	public void setPelicula (VOPelicula pelicula) {
		this.pelicula = pelicula;
	}
	
	public VOPelicula getPelicula() {
		return pelicula;
	}
	
	public void setPrioridad (double prioridad) {
		this.prioridad = prioridad;
	}
	
	public double getPrioridad() {
		return prioridad;
	}
	
	@Override
	public int compareTo(VOPrioridad o) {
		return (int)(1000*(this.prioridad - o.prioridad));
	}
}