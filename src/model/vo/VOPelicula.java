package model.vo;

import java.util.Date;

import model.data_structures.ILista;
import model.data_structures.LinearProbingHashST;
import model.data_structures.SeparateHash;


public class VOPelicula {
	
	private long ID;
	
	/*
	 * nombre de la pel�cula
	 */
	private String nombre;
	
	/*
	 * Fecha de lanzamiento de la pel�cula
	 */
	private Date fechaLanzamineto;
	
	/*
	 * Lista con los generos asociados a la pel�cula
	 */
	
	private ILista<String> generosAsociados;
	
	/*
	 * votos totales sobre la pel�cula
	 */
	 private int votostotales;  
	 
	 /*
	  * promedio anual de votos (hasta el 2016)
	  */
	 private int promedioAnualVotos; 
	 
	 /*
	  * promedio IMBD
	  */
	 
	 private double ratingIMBD;	
	 
	 private String pais;
	 
	 private SeparateHash<Long,Double> peliculasSimilitud;
	 
	 private SeparateHash<Integer,Double> usuariosSimilitud;
	 
	 private LinearProbingHashST<Integer, VOTago> tags;
	 
	 public VOPelicula() {
		// TODO Auto-generated constructor stub
	}

	public long getID(){
		 return ID;
	}
	
	public void setID(long id){
		ID = id;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaLanzamineto() {
		return fechaLanzamineto;
	}

	public void setFechaLanzamineto(Date fechaLanzamineto) {
		this.fechaLanzamineto = fechaLanzamineto;
	}

	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}

	public void setGenerosAsociados(ILista<String> listaGeneros) {
		this.generosAsociados = listaGeneros;
	}

	public int getVotostotales() {
		return votostotales;
	}

	public void setVotostotales(int votostotales) {
		this.votostotales = votostotales;
	}

	public int getPromedioAnualVotos() {
		return promedioAnualVotos;
	}

	public void setPromedioAnualVotos(int promedioAnualVotos) {
		this.promedioAnualVotos = promedioAnualVotos;
	}

	public double getRatingIMBD() {
		return ratingIMBD;
	}

	public void setRatingIMBD(double ratingIMBD) {
		this.ratingIMBD = ratingIMBD;
	}
	 
	public SeparateHash<Long, Double> getSimilitudes(){
		return peliculasSimilitud;
	}
	
	public void setSimilitudes(SeparateHash<Long, Double> datosSimilitud){
		this.peliculasSimilitud = datosSimilitud;
	}
	public SeparateHash<Integer, Double> getUsuarios(){
		return usuariosSimilitud;
	}
	
	public void setUsuarios(SeparateHash<Integer, Double> datosSimilitud){
		this.usuariosSimilitud = datosSimilitud;
	}
	public LinearProbingHashST<Integer, VOTago> getTags(){
		return tags;
	}
	public void setTags(VOTago tag){
		if(tags==null){
			tags = new LinearProbingHashST<>();
			this.tags.put(tag.getUserId(), tag);

		}
		this.tags.put(tag.getUserId(), tag);
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String p) {
		pais = p;
	}
}
