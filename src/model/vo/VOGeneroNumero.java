package model.vo;

import api.IComparable;

public class VOGeneroNumero implements Comparable<VOGeneroNumero>{
	private String genero;
	private int numInt;
	private double numDouble;
	
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getGenero() {
		return genero;
	}
	public void setInt(int numInt) {
		this.numInt = numInt;
	}
	public int getInt() {
		return numInt;
	}
	public void setDouble(double numDouble) {
		this.numDouble = numDouble;
	}
	public double getDouble() {
		return this.numDouble;
	}
	@Override
	public int compareTo(VOGeneroNumero o) {
		if(numInt != 0) {
			return (int)(o.numInt - numInt);	
		} else {
			return (int)(1000*(o.numDouble - numDouble));
		}
	}
}
