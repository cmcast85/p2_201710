package model.vo;

public class VOPrueba implements Comparable<VOPrueba>{

	private String nombre;
	
	private int ano;

	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public void setAno(int ano){
		this.ano = ano;
	}
	
	public int getAno(){
		return ano;
	}
	
	public String getNombre(){
		return nombre;
	}
	
	@Override
	public int compareTo(VOPrueba o) {
		if(ano - o.ano == 0){
			return nombre.compareTo(o.nombre);
		}
		else return ano - o.ano;
	}
	
	public String toString(){
		return nombre + " " + ano;
	}
	
	
}
