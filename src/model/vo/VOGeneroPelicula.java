package model.vo;

import java.util.Date;

import model.data_structures.RedBlackBST;

public class VOGeneroPelicula {

	/*
	 * nombre del genero
	 */
	
	private String genero;
	
	private RedBlackBST<Date, VOPelicula> peliculas;
	

	
	public String getNombre() {
		return genero;
	}
	
	public void setNombre(String nombre) {
		this.genero = nombre;
	}
	
	public RedBlackBST<Date, VOPelicula> getPeliculas(){
		return peliculas;
	}
	public void setPeliculas(RedBlackBST<Date, VOPelicula> peliculas){
		this.peliculas = peliculas;
	}
}
