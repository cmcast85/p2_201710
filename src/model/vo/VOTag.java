package model.vo;

public class VOTag {
	
	/**
	 * contenido del tag
	 */
	private String contenido;
	
	private String clase;
	
	public VOTag() {
		// TODO Auto-generated constructor stub
	}
	
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public String getClase(){
		return clase;
	}
	
	public void setClase(String clase) {
		this.clase = clase;
	}
	
}
