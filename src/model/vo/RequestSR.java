package model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestSR {

	@SerializedName("user_name")
	@Expose
	private String userName;
	
	@SerializedName("ratings")
	@Expose
	private RatingUsuario[] ratings;
	
	public String getName(){
		return userName;
	}
	public void setName(String name){
		userName = name;
	}
	public RatingUsuario[] getRatings(){
		return ratings;
	}
	public void setRatings(RatingUsuario[] rat){
		ratings = rat;
	}
}
