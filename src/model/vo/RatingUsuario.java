package model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingUsuario {

	@SerializedName("item_id")
	@Expose
	private int id;
	
	@SerializedName("rating")
	@Expose
	private double rating;
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	
	public double getRating(){
		return rating;
	}
	public void setRating(double rating){
		this.rating = rating;
	}
	
	public String toString(){
		return "Id: " + id + "rating: " + rating;
	}
}
