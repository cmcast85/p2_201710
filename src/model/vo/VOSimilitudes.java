package model.vo;

public class VOSimilitudes {

	private double recomendado;
	
	private long id;
	
	public double getRecomendado(){
		return recomendado;
	}
	public void setRecomendado(double recomendado){
		this.recomendado = recomendado;
	}
	public long getID(){
		return id;
	}
	public void setID(long id){
		this.id = id;
	}
}
