package model.vo;

public class VOTago {
private String tag;
	
	private long timestamp;
	
	private Integer userId;
	
	private long movieId;
	
	private String segmento;
	
	public String getTag() {
		return tag;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public long getMovieId() {
		return movieId;
	}
	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getSegmento(){
		return segmento;
	}
	public void setSegmento(String segmento){
		this.segmento = segmento;
	}
	public int compareTo(VOTago o, int criterio) {
		switch (criterio) {
		case -1: return (int)(o.timestamp - timestamp);
		}
		return 0;
	}
}
