package model.vo;

import model.data_structures.ListaEncadenada;
import model.data_structures.SeparateHash;

public class VOUsuario implements Comparable<VOUsuario> {

	private int idUsuario;	
	
	private long primerTimestamp;
	
	private int numRatings;
	
	private double diferenciaOpinion;
	
	private SeparateHash<Long, Double> rated;

	private SeparateHash<Long, Double> error;
	
	private int no_clasificado;
	
	private int conforme;
	
	private int inconforme;
	
	private int neutro;
	
	private int numTags;
	
	private ListaEncadenada<VOTago> tags;
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	public SeparateHash<Long, Double> getRated(){
		return rated;
	}
	public void setRated(SeparateHash<Long, Double> rate){
		this.rated = rate;
	}

	public SeparateHash<Long, Double> getError() {
		return error;
	}
	
	public void setError(SeparateHash<Long, Double> error) {
		this.error = error;
	}
	
	public ListaEncadenada<VOTago> getTags() {
		return tags;
	}
	
	public void setTags(ListaEncadenada<VOTago> tags) {
		this.tags = tags;
	}
	
	public String getSegmento(){
		if(no_clasificado==0 && inconforme == 0 && neutro==0){
			return "no_clasificado";
		}
		else if(conforme>inconforme&&conforme>neutro){
			return "conforme";
		}
		else if(inconforme>conforme&&inconforme>neutro){
			return "inconforme";
		}
		else if(neutro>conforme&&neutro>inconforme){
			return "neutro";
		}
		else return "empate";
	}
	
	public void setSegmento(String segmento){
		if(segmento==null) ++no_clasificado;
		else
		switch(segmento){
		case "no_clasificado": ++no_clasificado;
		break;
		case "conforme": ++conforme;
		break;
		case "inconforme": ++inconforme;
		break;
		case "neutro": ++neutro;
		default : ++no_clasificado;
		}
	}
	
	public int getNumTags(){
		return numTags;
	}
	public void setNumTags(int numTags){
		this.numTags = numTags;
	}
	
	public int compareTo(VOUsuario o, int criterio) {
		if(this.primerTimestamp - o.primerTimestamp == 0){
			if(this.numRatings - o.numRatings ==0){
				if(this.numTags - o.numTags == 0)
				{
					return 0;
				}
				else return this.numTags - o.numTags;
			}
			else return this.numRatings - o.numRatings;
		}
		else return (int)this.primerTimestamp - (int)o.primerTimestamp;
	}
	public int compare(VOUsuario o1, VOUsuario o2) {
		if(o1.primerTimestamp - o2.primerTimestamp == 0){
			if(o1.numRatings - o2.numRatings ==0){
				if(o1.numTags - o2.numTags == 0)
				{
					return 0;
				}
				else return o1.numTags - o2.numTags;
			}
			else return o1.numRatings - o2.numRatings;
		}
		else return (int)o1.primerTimestamp - (int)o2.primerTimestamp;
		
	}
	@Override
	public int compareTo(VOUsuario o) {
		if(this.primerTimestamp - o.primerTimestamp == 0){
			if(this.numRatings - o.numRatings ==0){
				if(this.numTags - o.numTags == 0)
				{
					return 0;
				}
				else return this.numTags - o.numTags;
			}
			else return this.numRatings - o.numRatings;
		}
		else return (int)this.primerTimestamp - (int)o.primerTimestamp;
	}
	
	
	
}
