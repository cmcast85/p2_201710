package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class SortString {

    public ILista<String> mergeSort(ILista<String> arreglo) {
    	arreglo.restablecerActual();
    	ListaEncadenada<String> aRetornar = new ListaEncadenada<>();
    	int tamano = arreglo.darNumeroElementos();
        if (tamano > 1) {

            int size1 = (int)Math.floor(tamano / 2);
            int size2 = tamano - size1;

            ListaEncadenada<String> left = new ListaEncadenada<>();
            ListaEncadenada<String> right = new ListaEncadenada<>();
            for (int i = 0; i < size1; i++) {

                left.agregarElementoFinal(arreglo.darElementoPosicionActual());
                arreglo.avanzarSiguientePosicion();

            }

            for (int i = 0; i < size2; i++)  {

                right.agregarElementoFinal(arreglo.darElementoPosicionActual());
                arreglo.avanzarSiguientePosicion();

            }

            left = (ListaEncadenada<String>) mergeSort(left);

            right = (ListaEncadenada<String>) mergeSort(right);
            aRetornar = (ListaEncadenada<String>) merge(left, right);

        } else return arreglo;
        
        return aRetornar;
    }

    private ILista<String> merge(ILista<String> left, ILista<String> right) {
    	
    	left.restablecerActual();
    	right.restablecerActual();
    	
    	ILista<String> result = new ListaEncadenada<String>();
    	
    	int tam = left.darNumeroElementos() + right.darNumeroElementos();
    	
        int i1 = 0;
        int i2 = 0;

        for (int i = 0; i < tam; i++) {
            if (i2 >= right.darNumeroElementos() || (i1 < left.darNumeroElementos() && left.darElementoPosicionActual().compareToIgnoreCase(right.darElementoPosicionActual()) <= 0)) {
                result.agregarElementoFinal(left.darElementoPosicionActual());
                left.avanzarSiguientePosicion();
                i1++;
            }
 
            else {
            	result.agregarElementoFinal(right.darElementoPosicionActual());
            	right.avanzarSiguientePosicion();
            	i2++;
            }
        }
        return result;
    }
 }