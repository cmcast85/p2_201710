package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import jdk.nashorn.internal.ir.RuntimeNode.Request;
import model.data_structures.Heap;
import model.data_structures.ILista;
import model.data_structures.LinearProbingHashST;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.MaxPQ;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateHash;
import model.vo.ImdbData;
import model.vo.JsonData;
import model.vo.RatingUsuario;
import model.vo.RequestSR;
import model.vo.VOGeneroNumero;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOPrioridad;
import model.vo.VORating;
import model.vo.VOReporteSegmento;
import model.vo.VOSimilitudes;
import model.vo.VOTag;
import model.vo.VOTago;
import model.vo.VOUsuario;
import model.vo.VOUsuarioPelicula;

public class SistemaRecomendacion {
	
	//private RedBlackBST<Long, String> arbol;
	
	private SeparateHash<Long, VOPelicula> peliculas;
	
	//private LinearProbingHashST<Long, String> lh;
	
	private ILista<VORating> ratings;
	
	private LinearProbingHashST<String, String> diccionarioTags;
	
	private LinearProbingHashST<Long, VOTago> tags;
	
	private LinearProbingHashST<Integer,VOUsuario> usuarios;
	
	private MaxPQ<VOUsuario> listaPrioridad;
	
	private int nuevoIdUsuario;
	
	private SeparateHash<String, VOGeneroPelicula> generoPeliculas;
	
	private RedBlackBST<Integer, SeparateHash<String, RedBlackBST<Double, ListaDobleEncadenada<VOPelicula>>>> ganasDeJoder;
	
	public boolean cargarJson(String link){
		try {
			JsonReader reader = new JsonReader(new BufferedReader(new FileReader(new File(link))));
			peliculas = new SeparateHash<>();
			Gson gson = new Gson();
			JsonData[] peliculas = gson.fromJson(reader, JsonData[].class);
			for (JsonData datos : peliculas) {
				VOPelicula pelicula = new VOPelicula();
				ImdbData datosI = datos.getImdbData();
				
				//ID
				long ID = Long.parseLong(datos.getMovieId());
				pelicula.setID(ID);
				
				//Nombre
				String nombre = datosI.getTitle();
				pelicula.setNombre(nombre);
				
				//Fecha lanzamiento
				DateTimeFormatter dTF = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.US);
				if(!datosI.getReleased().startsWith("N")){
					LocalDate lds = LocalDate.parse(datosI.getReleased(), dTF);
					@SuppressWarnings("deprecation")
					Date fechaLanzamineto = new Date(lds.getYear(), lds.getMonthValue(), lds.getDayOfMonth());
					pelicula.setFechaLanzamineto(fechaLanzamineto);
				}
				
				//Generos asociados
				ListaDobleEncadenada<String> generosAsociados = new ListaDobleEncadenada<>();
				String gens[] = datosI.getGenre().split(",");
				for (String genero : gens) {
					if(genero.startsWith(" ")) genero = genero.substring(1);
					generosAsociados.agregarElementoFinal(genero);
				}
				pelicula.setGenerosAsociados(generosAsociados);
				
				int votostotales = 0;
				//Votos totales
				if(!datosI.getImdbVotes().contains("N")){
					votostotales = Integer.parseInt(datosI.getImdbVotes().replace(",", ""));
					pelicula.setVotostotales(votostotales);
				}
				else{
					pelicula.setVotostotales(0);
				}
				
				//Promedio anual votos
				if(pelicula.getFechaLanzamineto()!=null && votostotales!=0){
					@SuppressWarnings("deprecation")
					int promedioAnualVotos = votostotales/(2018-pelicula.getFechaLanzamineto().getYear());
					pelicula.setPromedioAnualVotos(promedioAnualVotos);
					
				}
				
				//Rating imdb
				if(!datosI.getImdbRating().contains("N")){
					double ratingIMBD = Double.parseDouble(datosI.getImdbRating());
					pelicula.setRatingIMBD(ratingIMBD);
				}
				else{
					pelicula.setRatingIMBD(0);
				}
				
				//Pais
				String pais = datosI.getCountry();
				pelicula.setPais(pais);
				
				this.peliculas.put(pelicula.getID(), pelicula);
				
				
			}
			reader.close();
			
			//Cola Prioridad
			listaPrioridad = new MaxPQ<>(5);
			
			//Generos
			actualizarGeneros();
			
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/*
	 * TODO: revisar que tanto necesitamos la informacion de este, por que nos pondrian tags 
	 * con menos vainas
	 * 
	 */
	public boolean cargarTagsSR(String rutaTags, String ruta2) {
		cargarTags(ruta2);
		
		tags = new LinearProbingHashST<>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(rutaTags) );
			String temp;
			br.readLine();
			while((temp = br.readLine()) != null){
				String[] arr = new String[4];
				boolean comillas = false;
				
				//revisa si tiene " en el nombre
		        if(temp.contains("\"")) {
		        	comillas = true;
		        	int primeraComa = temp.indexOf(',');
		        	int segundaComa = temp.indexOf(',', primeraComa+1);
					arr[0] = temp.substring(0, primeraComa);
					arr[1] = temp.substring(primeraComa + 1, segundaComa);
					arr[2] = temp.substring(segundaComa + 2, temp.lastIndexOf(',')-1);
					arr[3] = temp.substring(temp.lastIndexOf(',') + 1);
		        }
		        else {
		        	arr = temp.split(",");
		        }
				if (!comillas) {
					
				}
				String tagsTemp[] = arr[2].split(",");
				for (String nombreTag : tagsTemp) {
					VOTago tag = new VOTago();
					if(nombreTag.startsWith(" ")) nombreTag = nombreTag.substring(1);
					tag.setTag(nombreTag);
					tag.setTimestamp(Long.parseLong(arr[3]));
					tag.setUserId(Integer.parseInt(arr[0]));
					tag.setMovieId(Long.parseLong(arr[1]));
					tag.setSegmento(diccionarioTags.get(nombreTag));
					
					peliculas.get(Long.parseLong(arr[1])).setTags(tag);
					usuarios.get(Integer.parseInt(arr[0])).setSegmento(tag.getSegmento());
					try {
						usuarios.get(Integer.parseInt(arr[0])).getTags().agregarElementoFinal(tag);
					} catch (Exception e) {
						ListaEncadenada<VOTago> tags = new ListaEncadenada<>();
						tags.agregarElementoFinal(tag);
						usuarios.get(Integer.parseInt(arr[0])).setTags(tags);
					}
					try {
						usuarios.get(Integer.parseInt(arr[0])).setNumTags(usuarios.get(Integer.parseInt(arr[0])).getNumTags()+1);
					} catch (Exception e) {
						usuarios.get(Integer.parseInt(arr[0])).setNumTags(1);
					}
					tags.put(Long.parseLong(arr[3]), (tag));
				}
				
			}
			
			br.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean cargarRatings(String csv){
		try {
			BufferedReader br = new BufferedReader(new FileReader(csv));
			ratings = new ListaEncadenada<VORating>();
			String temp;
			int ultimoUsuario = 0;
			usuarios = new LinearProbingHashST<Integer, VOUsuario>();
			br.readLine();
			while((temp = br.readLine()) != null){
				
				//Ratings
				VORating rating = new VORating();
				
				String[] temp2 = temp.split(",");
				
				rating.setIdUsuario(Integer.parseInt(temp2[0]));
				rating.setIdPelicula(Long.parseLong(temp2[1]));
				rating.setRating(Double.parseDouble(temp2[2]));
				rating.setTimestamp(Long.parseLong(temp2[3]));
				rating.setError(Double.parseDouble(temp2[4]));
				
				ratings.agregarElementoFinal(rating);

				//Usuario
				if (rating.getIdUsuario() > ultimoUsuario) {

					ultimoUsuario = rating.getIdUsuario();
					nuevoIdUsuario = ultimoUsuario + 1;
					
					VOUsuario usuario = new VOUsuario();
					usuario.setIdUsuario(rating.getIdUsuario());
					usuario.setPrimerTimestamp(rating.getTimestamp());
					usuario.setNumRatings(1);
					
					SeparateHash<Long, Double> rate = new SeparateHash<>();
					rate.put(rating.getIdPelicula(), rating.getRating());
					usuario.setRated(rate);
					
					SeparateHash<Long, Double> error = new SeparateHash<>();
					error.put(rating.getIdPelicula(), rating.getError());
					usuario.setError(error);
					
					usuarios.put(usuario.getIdUsuario(), usuario);
					
					VOPelicula peli  = buscarPelicula(rating.getIdPelicula());
					SeparateHash<Integer, Double> us;
					if ((us = peli.getUsuarios()) != null) {
						us.put(usuario.getIdUsuario(), rating.getRating());
					} else {
						SeparateHash<Integer, Double> us2 = new SeparateHash<>();
						us2.put(usuario.getIdUsuario(), rating.getRating());
						peli.setUsuarios(us2);
					}
				} else {
					VOUsuario usuario = usuarios.get(rating.getIdUsuario());
					if(usuario.getPrimerTimestamp() < Long.parseLong(temp2[3])) {
						usuario.setPrimerTimestamp(Long.parseLong(temp2[3]));
					}
					usuario.setNumRatings(usuario.getNumRatings() + 1);
					usuario.getRated().put(rating.getIdPelicula(), rating.getRating());
					usuario.getError().put(rating.getIdPelicula(), rating.getError());
					
					VOPelicula peli  = buscarPelicula(rating.getIdPelicula());
					SeparateHash<Integer, Double> us;
					if ((us = peli.getUsuarios()) != null) {
						us.put(usuario.getIdUsuario(), rating.getRating());
					} else {
						SeparateHash<Integer, Double> us2 = new SeparateHash<>();
						us2.put(usuario.getIdUsuario(), rating.getRating());
						peli.setUsuarios(us2);
					}
				}
			}	
			
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		similitudes();
		return true;
	}
	
	private boolean cargarTags(String csv){
		try {
			BufferedReader br = new BufferedReader(new FileReader(csv));
			diccionarioTags = new LinearProbingHashST<>();
			String temp;
			while((temp = br.readLine()) != null){
				String[] temp2 = temp.split(",");
				
				String nombre =(temp2[0]);
				String segmento= (temp2[1]);
				
				diccionarioTags.put(nombre, segmento);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private void similitudes() {
		Iterable<Long> iter = peliculas.keys();
		for(long peli : iter){
			SeparateHash<Long, Double> similitudes = new SeparateHash<>();
			for(long peli2 : iter){
				double t1 =0;
				double t2 = 0;
				double t3 = 0;
				if(peli!=peli2){
					if(peliculas.get(peli).getUsuarios()==null) break;
					Iterable<Integer> iter2 = peliculas.get(peli).getUsuarios().keys();
					for(int k : iter2){
						try{
							
							t1 += peliculas.get(peli).getUsuarios().get(k) *peliculas.get(peli2).getUsuarios().get(k);
							t2 += Math.pow(peliculas.get(peli).getUsuarios().get(k),2);
							t3 += Math.pow(peliculas.get(peli2).getUsuarios().get(k), 2);
						}
						catch(Exception e){
							
						}
					}//Sumatoria
				}
				similitudes.put(peli2, t1/(Math.sqrt(t2)*Math.sqrt(t3)));
			}
			peliculas.get(peli).setSimilitudes(similitudes);
		}
	}
	
	private VOPelicula buscarPelicula(Long id) {
		return peliculas.get(id);
	}
	
	private void actualizarGeneros() {
		//Carga la lista de generos y las peliculas de ese genero
		
	    ILista<String> listagen = new ListaEncadenada<String>();
	    Iterable<Long> i = peliculas.keys();
	    for(Long ite : i){
	      VOPelicula iter = buscarPelicula(ite); 
	    	for(String gen : iter.getGenerosAsociados()){
	        listagen.agregarElementoFinalNorepetido(gen);
	      }
	    }
	    
	    VOGeneroPelicula genero;
	    generoPeliculas = new SeparateHash<>();
	    String ultimoGenero = "wololo";
	    for(String gn : listagen){
	    	if(!gn.equals(ultimoGenero)) {
		    	genero = new VOGeneroPelicula();
		    	genero.setNombre(gn);

				RedBlackBST<Date, VOPelicula> tempo ;
				tempo = new RedBlackBST<>();
				Iterable<Long> j = peliculas.keys();
				for( Long ite : j ){
					VOPelicula iter = buscarPelicula(ite); 
					for(String t : iter.getGenerosAsociados()){
						if(t.equals(gn)&& iter.getFechaLanzamineto()!=null) tempo.put(iter.getFechaLanzamineto(), iter);
					}
				}
				genero.setPeliculas(tempo);
				generoPeliculas.put(genero.getNombre() , genero);
	    	}
	    	ultimoGenero = gn;
	    }
	}
	
	public void requerimiento1(int id){

		VOUsuario usuario = usuarios.get(id);
		listaPrioridad.insert(usuario);
	}
	
	public void requerimiento1(String json){
		try {
			JsonReader reader = new JsonReader(new BufferedReader(new FileReader(new File(json))));
			Gson gson = new Gson();
			
			RequestSR request = gson.fromJson(reader, RequestSR.class);
			
			RatingUsuario[] rus = request.getRatings();
			SeparateHash<Integer, Double> sh = new SeparateHash<>();
			VOUsuario nuevo = new VOUsuario();
			nuevo.setIdUsuario(nuevoIdUsuario);
			nuevo.setPrimerTimestamp(Long.MAX_VALUE);
			System.out.println(rus.toString());
			for(RatingUsuario ru : rus){
				int id = ru.getId();
				double rating = ru.getRating();
				sh.put(id, rating);
				
			}
			usuarios.put(nuevoIdUsuario++, nuevo);
			listaPrioridad.insert(nuevo);
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public String requerimiento2(){

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String fecha = dateFormat.format(date).toString();
		try {
			FileWriter fl = new FileWriter(new File("./Data/rta.json"));
			
			int i = 0;
			ILista<VOSimilitudes> lista = new ListaEncadenada<>();
			while(i<10&&!listaPrioridad.isEmpty()){
				
				lista = requerimiento2(listaPrioridad.delMax().getIdUsuario());
			}
			Gson gson = new GsonBuilder().create();
		    gson.toJson(lista, fl);
			return 	"./Data/rta.json";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	private ILista<VOSimilitudes> requerimiento2(int id){
		Iterable<Long> iter = peliculas.keys();
		MaxPQ<VOSimilitudes> pq = new MaxPQ<VOSimilitudes>();
		for(long i : iter){
			VOSimilitudes sm = new VOSimilitudes();
			
			double end = 0;
			if(peliculas.get(i).getUsuarios().get(id) !=null){
				end = calcularRating(id,i);
				}
				
				sm.setID(i);
				sm.setRecomendado(end);
				pq.insert(sm);
			
		}
		ILista<VOSimilitudes> retorno = new ListaEncadenada<>();
		int i = 0;
		while(i<5&&!pq.isEmpty()){
			retorno.agregarElementoFinal(pq.delMax());
			i++;
		}
		return retorno;
	}
	
	private double calcularRating(int idUsuario, long idPelicula){
		double uno =0;
		double dos = 0;
		double end = 0;
		for(long pelicula : usuarios.get(idUsuario).getRated().keys()) {
			if (pelicula != idPelicula) {
				System.out.println("-Pelicula: " + pelicula);
				System.out.println("-Similitud: " + peliculas.get(idPelicula).getSimilitudes().get(pelicula));
				System.out.println("-Rating: " + usuarios.get(idUsuario).getRated().get(pelicula));
				uno += (peliculas.get(idPelicula).getSimilitudes().get(pelicula) * usuarios.get(idUsuario).getRated().get(pelicula));
				dos += peliculas.get(idPelicula).getSimilitudes().get(pelicula);
			}
		}
		end = uno/dos;
		return end;
	}
	
	public ILista<VOPelicula> requerimiento3(String genero, String inicio, String finale){
		VOGeneroPelicula gen = generoPeliculas.get(genero);
		ILista<VOPelicula> lista = new ListaEncadenada<>();
		DateTimeFormatter dTF = DateTimeFormatter.ofPattern("dd MMM uuuu");
		LocalDate lds = LocalDate.parse(inicio, dTF);
		@SuppressWarnings("deprecation")
		Date in = new Date(lds.getYear(), lds.getMonthValue(), lds.getDayOfMonth());
		lds = LocalDate.parse(finale, dTF);
		@SuppressWarnings("deprecation")
		Date fin = new Date(lds.getYear(), lds.getMonthValue(), lds.getDayOfMonth());
		Iterable<Date> iter = gen.getPeliculas().keys(in, fin);
		for(Date pel : iter ){
			lista.agregarElementoFinal(gen.getPeliculas().get(pel));
			System.out.println(gen.getPeliculas().get(pel).getNombre());
		}
		return lista;
	}
	
	public void requerimiento4(int idUsuario, double rating, long idPelicula) {
		double error = Math.abs(calcularRating(idUsuario, idPelicula) - rating);
		usuarios.get(idUsuario).getRated().put(idPelicula, rating);
		usuarios.get(idUsuario).getError().put(idPelicula, error);
	}
	
	public ListaEncadenada<VOUsuarioPelicula> requerimiento5(int idUsuario) {
		ListaEncadenada<VOUsuarioPelicula> retornar = new ListaEncadenada<>();
		System.out.println("---Usuario: " + idUsuario);
		for (long idPelicula : usuarios.get(idUsuario).getRated().keys()) {
			
			VOUsuarioPelicula temp = new VOUsuarioPelicula();
			
			temp.setIdUsuario(idUsuario);
			temp.setNombrepelicula(peliculas.get(idPelicula).getNombre());
			System.out.println("---Id pelicula: " + idPelicula);
			temp.setRatingUsuario(usuarios.get(idUsuario).getRated().get(idPelicula));
			System.out.println("---Rating: " + usuarios.get(idUsuario).getRated().get(idPelicula));
			temp.setErrorRating(usuarios.get(idUsuario).getError().get(idPelicula));
			System.out.println("---Error: " + usuarios.get(idUsuario).getError().get(idPelicula));
			temp.setRatingSistema(calcularRating(idUsuario, idPelicula));
			System.out.println("---Prediccion rating: " + calcularRating(idUsuario, idPelicula));
			temp.setTags(usuarios.get(idUsuario).getTags());
			
			retornar.agregarElementoFinal(temp);
		}
		return retornar;
	}
	
	public ILista<VOUsuario> requerimiento6(String segmento){
		ILista<VOUsuario> us = new ListaEncadenada<>();
		Iterable<Integer> iter = usuarios.keys();
		for(int it : iter){
			VOUsuario usuario = usuarios.get(it);
			if(usuario.getSegmento().equals(segmento)){
				us.agregarElementoFinal(usuario);
			}
		}
		return us;
	}
	
	@SuppressWarnings("deprecation")
	public void requerimiento7(){
		ganasDeJoder = new RedBlackBST<Integer,SeparateHash<String, RedBlackBST<Double, ListaDobleEncadenada<VOPelicula>>>>();
		
		for(long i : peliculas.keys()){
			VOPelicula actual = peliculas.get(i);
			
			System.out.println(actual.getGenerosAsociados().darElemento(0));
			SeparateHash<String, RedBlackBST<Double, ListaDobleEncadenada<VOPelicula>>> sh;
			if(actual.getFechaLanzamineto()!=null&&ganasDeJoder.get(actual.getFechaLanzamineto().getYear())==null){
				sh = new SeparateHash<String, RedBlackBST<Double, ListaDobleEncadenada<VOPelicula>>>();
				ganasDeJoder.put(actual.getFechaLanzamineto().getYear(), sh);
			
			
				sh = ganasDeJoder.get(actual.getFechaLanzamineto().getYear());
				RedBlackBST<Double, ListaDobleEncadenada<VOPelicula>> rbt;
				if(actual.getGenerosAsociados().darElemento(0)!=null
					||sh.get(actual.getGenerosAsociados().darElemento(0))==null){
				rbt = new RedBlackBST<Double, ListaDobleEncadenada<VOPelicula>>();
				sh.put(actual.getGenerosAsociados().darElemento(0), rbt);
				rbt = sh.get(actual.getGenerosAsociados().darElemento(0));
				ListaDobleEncadenada<VOPelicula> lista;
				if(rbt.get(actual.getRatingIMBD())==null){
					lista = new ListaDobleEncadenada<VOPelicula>();
					rbt.put(actual.getRatingIMBD(), lista);
				}
				lista = rbt.get(actual.getRatingIMBD());
				lista.agregarElementoFinal(actual);
				}
			}
			
		}
		
	}

	public VOReporteSegmento requerimiento8 (String segmento) {
		VOReporteSegmento retornar = new VOReporteSegmento();
		LinearProbingHashST<String, Integer> generosCantidad = new LinearProbingHashST<>();
		LinearProbingHashST<String, Double> generosPromedio = new LinearProbingHashST<>();
		int numPeliculas = 0;
		double totalErrores = 0;
		
		for(int id : usuarios.keys()) {
			System.out.println(usuarios.get(id).getSegmento());
			if (usuarios.get(id).getSegmento().equals(segmento)) {
				
				for (long idPelicula : usuarios.get(id).getRated().keys()) {
					totalErrores += usuarios.get(id).getError().get(idPelicula);
					numPeliculas++;
					
					for (String genero : peliculas.get(idPelicula).getGenerosAsociados()) {
						
						if(generosCantidad.contains(genero)) {
							generosCantidad.put(genero, generosCantidad.get(genero) + 1);
						} else generosCantidad.put(genero, 1);
						
						if (generosPromedio.contains(genero)) {
							generosPromedio.put(genero, generosPromedio.get(genero) + usuarios.get(id).getRated().get(idPelicula));
						} else generosPromedio.put(genero, usuarios.get(id).getRated().get(idPelicula));
					} 
				}
			}
		}

		retornar.setErrorPromedio(totalErrores/numPeliculas);
		VOGeneroNumero[] listaConteo = new VOGeneroNumero[generosPromedio.size()];
		VOGeneroNumero[] listaPromedio = new VOGeneroNumero[generosPromedio.size()];
		int pos = 0;
		for (String gen : generosPromedio.keys()) {
			generosPromedio.put(gen, generosPromedio.get(gen)/generosCantidad.get(gen));
			VOGeneroNumero cont = new VOGeneroNumero();
			cont.setGenero(gen);
			cont.setInt(generosCantidad.get(gen));
			System.out.println("\t\t\t\tConteo ratings--" + gen + ": " + generosCantidad.get(gen));
			listaConteo[pos] = cont;
			VOGeneroNumero prom = new VOGeneroNumero();
			prom.setGenero(gen);
			prom.setDouble(generosPromedio.get(gen));
			System.out.println("Promedio--" + gen + ": " + generosPromedio.get(gen));
			listaPromedio[pos] = prom;
			pos++;
		}
		Heap.sort(listaConteo);
		Heap.sort(listaPromedio);
		ListaEncadenada<VOGeneroPelicula> finalCont = new ListaEncadenada<>();
		ListaEncadenada<VOGeneroPelicula> finalProm = new ListaEncadenada<>();
		for (int i = 0; i < 5; i++) {
			finalCont.agregarElementoFinalNorepetido(generoPeliculas.get(listaConteo[i].getGenero()));
			finalProm.agregarElementoFinalNorepetido(generoPeliculas.get(listaPromedio[i].getGenero()));
		}
		retornar.setGenerosMasRatings(finalCont);
		retornar.setGenerosMejorPromedio(finalProm);
		System.out.println("Error promedio: " + retornar.getErrorPromedio());
		System.out.println("Generos m�s ratings: ");
		for (int i = 0; i < finalCont.darNumeroElementos(); i++) {
			System.out.println(listaConteo[i].getGenero());
			System.out.println("--Genero: " + finalCont.darElemento(i).getNombre());
		}
		System.out.println("Generos mejor promedio: ");
		for (int i = 0; i < finalCont.darNumeroElementos(); i++) {
			System.out.println(listaPromedio[i].getGenero());
			System.out.println("--Genero: " + finalProm.darElemento(i).getNombre());
		};
		return retornar;
	}
	
	public ILista<VOPelicula> requerimiento9(String genero, String fechaInicial, String fechaFinal) {
		ListaEncadenada<VOPelicula> peliculas = new ListaEncadenada<>();
		DateTimeFormatter dTF = DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.US);
		LocalDate lds = LocalDate.parse(fechaInicial, dTF);
		@SuppressWarnings("deprecation")
		Date inicio = new Date(lds.getYear(), lds.getMonthValue(), lds.getDayOfMonth());
		lds = LocalDate.parse(fechaFinal, dTF);
		@SuppressWarnings("deprecation")
		Date fin = new Date(lds.getYear(), lds.getMonthValue(), lds.getDayOfMonth());
		
		for(Date date: generoPeliculas.get(genero).getPeliculas().keys(inicio,fin)) {
			peliculas.agregarElementoFinal(generoPeliculas.get(genero).getPeliculas().get(date));
		}
		return peliculas;
	}
	
	public MaxPQ<VOPrioridad> requerimiento10(int n) {
		MaxPQ<VOPrioridad> retornar = new MaxPQ<>();
		LinearProbingHashST<Long, Integer> peliculasRatings = new LinearProbingHashST<>();
		for (VORating rating : ratings) {
			long idPelicula = rating.getIdPelicula();
			if (peliculasRatings.contains(idPelicula)) {
				peliculasRatings.put(idPelicula, peliculasRatings.get(idPelicula) + 1);
			} else peliculasRatings.put(idPelicula, 1);
		}
		for (long idPelicula : peliculas.keys()) {
			peliculas.get(idPelicula).setVotostotales(peliculasRatings.get(idPelicula));
			double prioridad = (peliculas.get(idPelicula).getVotostotales() * peliculas.get(idPelicula).getRatingIMBD());
			VOPrioridad nuevo = new VOPrioridad();
			nuevo.setPelicula(peliculas.get(idPelicula));
			nuevo.setPrioridad(prioridad);
			retornar.insert(nuevo);
		}
		return retornar;
	}
	
	public SeparateHash<Long,VOPelicula> requerimiento11(int anio, String pais,String genero){
		SeparateHash<Long, VOPelicula> retorno = new SeparateHash<>();
		SeparateHash<Long, VOPelicula> temp1 = new SeparateHash<>();
		SeparateHash<Long, VOPelicula> temp2 = new SeparateHash<>();
		
		if(anio != 0){
			temp1 = filtrarAnio(anio,peliculas);
			if(pais!=null){
				temp2 = filtrarPais(pais, temp1);
				if(genero!=null){
					retorno = filtrarGenero(genero,temp2);
				}//filtra_3
			}//filtra_pais_anio
			else if(genero!=null){
				retorno = filtrarGenero(genero, temp1);
			}//filtra_genero_anio
		}//filra_año
		else if(pais!=(null)){
			temp1 = filtrarPais(pais, peliculas);
			if(genero!=null){
				retorno = filtrarGenero(genero, temp1);
			}
		}
		else if(genero!=(null)){
			retorno = filtrarGenero(genero, peliculas);
		}
		return retorno;
	}
	
	private SeparateHash<Long, VOPelicula> filtrarGenero(String genero, SeparateHash<Long, VOPelicula> temp){
		SeparateHash<Long, VOPelicula> ret = new SeparateHash<>();
		Iterable<Long> iter2 = temp.keys();
		for(long i : iter2){
			VOPelicula pel = peliculas.get(i);
			ILista<String> gns = pel.getGenerosAsociados();
			for(String gn : gns){
				if(gn.equals(genero)){
					ret.put(pel.getID(), pel);
					break;
				}				
			}
		}
		return ret; 
	}
	
	private SeparateHash<Long, VOPelicula> filtrarPais(String pais,SeparateHash<Long, VOPelicula> temp){
		SeparateHash<Long, VOPelicula> ret = new SeparateHash<>();
		Iterable<Long> iter2 = temp.keys();
		for(long i : iter2){
			VOPelicula pel = peliculas.get(i);
			if(pel.getPais().equals(pais)){
				ret.put(pel.getID(), pel);
			}
		}
		return ret; 
	}
	
	private SeparateHash<Long, VOPelicula> filtrarAnio(int anio, SeparateHash<Long, VOPelicula> peliculas2){
		SeparateHash<Long, VOPelicula> ret = new SeparateHash<>();
		Iterable<Long> iter = peliculas2.keys();
		for(long i : iter){
			VOPelicula pel = peliculas2.get(i);
			if(pel.getFechaLanzamineto()!=null&&pel.getFechaLanzamineto().getYear()==anio){
				ret.put(pel.getID(), pel);
			}
		}
		return ret; 
	}
	
}
