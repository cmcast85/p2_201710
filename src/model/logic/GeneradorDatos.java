package model.logic;

public class GeneradorDatos {

	
	public String[] generarCadena(int n){
		String[] ret = new String[n];
		
		for(int i = 0; i<n ;i++){
			
			StringBuilder sb = new StringBuilder();
			int j = 0;
			while(j<4){
				int r = (int) Math.floor(Math.random() * ((90-65)+1) + 65);
				char c = (char)r;
				sb.append(c);
				j++;
			}
			String el = sb.toString();
			ret[i] = el;
		}
		return ret;
	}
	
	public int[] generarAno(int n){
		int ret[] = new int[n];
		
		for(int i = 0;i<n;i++){
			int r = (int) Math.floor(Math.random() * ((2017-1950)+1) + 1950);
			
			
			ret[i] = r;
		}
		
		return ret;
	}
}
